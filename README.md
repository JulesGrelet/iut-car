# iut-car

> Site de covoiturage pour les étudiants et le personnel de l'université d'Orléans

## Installation base de données
1. Créez un projet Hasura (https://hasura.io/) et supprimer HASURA_GRAPHQL_ADMIN_SECRET dans "Env vars" dans les paramètres du projet Hasura
2. Puis lancez la console
3. Création de la base de données Heroku dans l'onget DATA
![alt text](./images_tuto/heroku.png)
4. Création des tables dans DATA > SQL
![alt text](./images_tuto/data.png)
5. Exécutez le "script creation_base_postgreSQL.sql"
6. Cliquez sur le schéma où vous avez généré les tables
7. Cliquez sur "Track All" des tables
![alt text](./images_tuto/track_all_t.png)
8. Puis sur le "Track All" des relations
![alt text](./images_tuto/track_all_r.png)

9. Changements dans la base de données initiale : dans hasura :
  - dans table 'notation' : cliquer sur Modify, dans Foreign Keys editer la première (itineraire) -> dans From remplacer utilisateurs par itineraire et To doit être l'id de cette table (itineraire), cliquer ensuite sur Save
  - dans table 'itineraire' : cliquer sur Modify, dans Columns ajouter une colonne -> nom : date, type : character varying, nullable : coché, cliquer ensuite sur Save

## Installation de l'application
1. Clone du projet sur gitlab
``` bash
git clone https://gitlab.com/nmartins/iut-car.git
```
2. Changez le lien de l'API GraphQL du fichier src/main.js par le lien de votre projet Hasura
```javascript
const httpLink = new HttpLink({
  uri: '<nouveau lien>'
});
```
![alt text](./images_tuto/lien.png)

3. Installation des modules et lancement de l'application
``` bash
# install dependencies
npm install

#fix node_module error
Ouvir le fichier lib.esm a la destination:
nodes_modules>bootstrap-icons-vue-dist
dans le première ligne, remplacez le 'h' par 'VUE'
sauvegardez et relancez le 'npm install'

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```
For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
