DROP TABLE IF EXISTS "notation";
DROP TABLE IF EXISTS "utilisateurs_itineraire";
DROP TABLE IF EXISTS "itineraire";
DROP TABLE IF EXISTS "utilisateurs";
DROP TABLE IF EXISTS "Voiture";

DROP EXTENSION IF EXISTS postgis_topology;
DROP EXTENSION IF EXISTS postgis;

CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS postgis_topology;

CREATE TABLE "utilisateurs" (
  "id" SERIAL PRIMARY KEY,
  "nom" varchar,
  "prenom" varchar,
  "password" varchar,
  "email" varchar,
  "adresse" varchar,
  "telephone" int,
  "description" text,
  "created_at" timestamptz,
  "permisConduire" boolean,
  "composante" varchar,
  "libelle" varchar,
  "nb_place" int,
  "photo" varchar,
  "marque" varchar,
  "modele" varchar,
  "assurance" timestamptz
);

CREATE TABLE "notation" (
  "itineraire" int,
  "utilisateurConducteur" int,
  "utilisateurPassager" int,
  "note" int,
  PRIMARY KEY ("utilisateurConducteur", "utilisateurPassager")
);

CREATE TABLE "itineraire" (
  "id" SERIAL PRIMARY KEY,
  "depart" varchar,
  "destination" varchar,
  "date_depart" timestamptz,
  "nb_passager" int,
  "temps_trajet" float,
  "description" text,
  "prix" float,
  "point_depart" point,
  "point_arrivee" point,
  "localisation_depart" GEOGRAPHY(Point),
  "localisation_arrivee" GEOGRAPHY(Point)
);

CREATE TABLE "utilisateurs_itineraire" (
  "id" SERIAL PRIMARY KEY,
  "utilisateur" int,
  "itineraire" int,
  "conducteur" boolean
);

ALTER TABLE "notation" ADD FOREIGN KEY ("itineraire") REFERENCES "utilisateurs" ("id");

ALTER TABLE "notation" ADD FOREIGN KEY ("utilisateurConducteur") REFERENCES "utilisateurs" ("id");

ALTER TABLE "notation" ADD FOREIGN KEY ("utilisateurPassager") REFERENCES "utilisateurs" ("id");

ALTER TABLE "utilisateurs_itineraire" ADD FOREIGN KEY ("utilisateur") REFERENCES "utilisateurs" ("id");

ALTER TABLE "utilisateurs_itineraire" ADD FOREIGN KEY ("itineraire") REFERENCES "itineraire" ("id");