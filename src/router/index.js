import Vue from 'vue'
import Router from 'vue-router'
import WelcomePage from '@/components/WelcomePage/WelcomePage'
import signInPage from '@/components/signInPage/signInPage'
import logInPage from '@/components/logInPage/logInPage'
import creationItinerairePage from '@/components/creationItinerairePage/creationItinerairePage'
import detailsItinerairePage from '@/components/detailsItinerairePage/detailsItinerairePage'
import listeItinerairePage from '@/components/listeItinerairePage/listeitinerairePage'
// import listeItinerairePageTest from '@/components/listeItinerairePageTest/listeItinerairePageTest'
import detailsUserPage from '@/components/detailsUserPage/detailsUserPage'
import usersListPage from '@/components/usersListPage/usersListPage'
import userProfilePage from '@/components/userProfilePage/userProfilePage'
import ProfilPage from '@/components/ProfilPage/ProfilPage'
import { authGuard } from '../auth/authGuard'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'WelcomePage',
      component: WelcomePage
    },
    {
      path: '/Inscription',
      name: 'signInForm',
      component: signInPage
    },
    {
      path: '/Connexion',
      name: 'logInForm',
      component: logInPage
    },
    {
      path: '/creationItineraire',
      name: 'creationItinerairePage',
      component: creationItinerairePage
    },
    {
      path: '/listeItineraire',
      name: 'listeItinerairePage',
      component: listeItinerairePage
    },
    {
      path: '/detailsItineraire/:id',
      name: 'detailsItinerairePage',
      component: detailsItinerairePage
    },
    {
      path: '/detailsUser/:userId',
      name: 'detailsUserPage',
      component: detailsUserPage,
      template: '',
      beforeEnter: authGuard
    },
    {
      path: '/Profil/',
      name: 'ProfilPage',
      component: ProfilPage,
      beforeEnter: authGuard
    },
    {
      path: '/UsersList/',
      name: 'usersListPage',
      component: usersListPage
    },
    {
      path: '/userProfile/:userId',
      name: 'userProfilePage',
      component: userProfilePage,
      beforeEnter: authGuard
    }
  ]
})
