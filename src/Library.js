/* eslint-disable */

const bcryptjs = require('bcryptjs')

export class Library {

  /**
   * Cette fonction permet transfomer une date en format textuel avec jour et heure
   * @param {string} date 
   */
  static transform_date_heure(date) {
    const event = new Date(date);
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' };
    return event.toLocaleString('fr-FR', options);
  }

  /**
   * Cette fonction permet transfomer une date en format textuel sans l'heure
   * @param {string} date 
   */
  static transform_date(date) {
    const event = new Date(date);
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    return event.toLocaleString('fr-FR', options);
  }

  /**
   * 
   * @param {Float} totalSeconds 
   * @returns Retourne le nombre de secondes passé en parametre sous forme h min
   */
  static get_duree_from_sec(totalSeconds) {
    var hours = Math.floor(totalSeconds / 3600);
    totalSeconds %= 3600;
    var minutes = Math.floor(totalSeconds / 60);
    if (hours > 0) {
      return hours + " h " + minutes + " min";
    }else{
      return minutes + " min";
    }
  }

  /**
   * Récupère des points gps via le geocoder de openstreetmap
   * @param {string} addr_depart 
   * @param {string} addr_arrivee 
   * @returns Retourne la latitude et la longitude des deux adresses passées en paramètre
   */
  static async get_lat_lon(addr_depart, addr_arrivee) {
    const depart_carte = await fetch(`https://nominatim.openstreetmap.org/search?q=${addr_depart}&format=json`)
    .then((response) => {
      return response.json()
    })
    .then((data) => {
      return {
        "type": "Point",
        "coordinates":[parseFloat(data[0].lat), parseFloat(data[0].lon)]
      };
    })
    .catch((error) => {
      console.log(error);
    })

    const arrivee_carte = await fetch(`https://nominatim.openstreetmap.org/search?q=${addr_arrivee}&format=json`)
    .then((response) => {
      return response.json()
    })
    .then((data) => {
      return {
        "type": "Point",
        "coordinates":[parseFloat(data[0].lat), parseFloat(data[0].lon)]
      };
    })
    .catch((error) => {
      console.log(error);
    })
    return [depart_carte, arrivee_carte]
  }

  /**
   * @param {string} password 
   * @returns Retourne le mot de passe hashé
   */
  static async hashPassword(password) {
    const salt = await bcryptjs.genSalt()
    password = await bcryptjs.hash(password, salt)
    return password
  }

}
