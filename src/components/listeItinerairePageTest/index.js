import gql from "graphql-tag"
import {Library} from '../../Library'
import listeItineraire from '@/components/listeItinerairePage/listeItinerairePage'

const GET_ITINERAIRE = gql`
query get_itineraire {
  itineraire {
    date_depart
    depart
    description
    destination
    id
    nb_passager
    prix
    temps_trajet
    point_depart
    point_arrivee
    date
  }
}
`;

const GET_ITINERAIRE_FILTER_DEPART = gql`
query get_itineraire_filter_depart ($value: String!) {
  itineraire(where: {depart: {_ilike: $value}}) {
    date_depart
    depart
    description
    destination
    id
    nb_passager
    prix
    temps_trajet
    point_depart
    point_arrivee
    date
  }
}
`;

const GET_ITINERAIRE_FILTER_DESTINATION = gql`
query get_itineraire_filter_destination ($value: String!) {
  itineraire(where: {destination: {_ilike: $value}}) {
    date_depart
    depart
    description
    destination
    id
    nb_passager
    prix
    temps_trajet
    point_depart
    point_arrivee
    date
  }
}
`;

const GET_ITINERAIRE_FILTER_DATE = gql`
query get_itineraire_filter_date ($value: String!) {
  itineraire(where: {date: {_eq: $value}}) {
    date_depart
    depart
    description
    destination
    id
    nb_passager
    prix
    temps_trajet
    point_depart
    point_arrivee
    date
  }
}
`;

export default {
  mounted(){
    this.$apollo.query({
      query: GET_ITINERAIRE,
    }).then(({data}) => {
      this.itineraireInfo = data.itineraire;
      let i = 0;
      for(let itineraire of this.itineraireInfo){
        let s = itineraire.date_depart
        this.itineraireInfo[i].date_depart = s[8]+s[9] +' / '+ s[5]+s[6]+ ' / '+s[0]+s[1]+s[2]+s[3]
        this.itineraireInfoId[i] = itineraire.id
        i++;
      }
    }).catch((error) => {
      console.log(error);
    });
  },
  data() {
    return {
      itineraireInfo: [],
      itineraireInfoId: [],
      filters: [
        { key: 'depart', value: null, enabled: false },
        { key: 'destination', value: null, enabled: false },
        { key: 'date', value: null, enabled: false },
        //{ key: 'prix', valueMin: null, valueMax: null, enabled: false }
      ]
    }
  },
  methods: {
    filter() {
      this.filters.forEach(f => {
        if (f.enabled && f.value!=null && f.value!="" && f.value!=" ") {
          let query = null;
          let value = null;
          if (f.key=="date")
            value = f.value
          else
            value = "%"+f.value+"%";
          switch(f.key) {
            case 'depart':query=GET_ITINERAIRE_FILTER_DEPART; break;
            case 'destination':query=GET_ITINERAIRE_FILTER_DESTINATION; break;
            case 'date':query=GET_ITINERAIRE_FILTER_DATE; break;
            //case 'prix':query=GET_ITINERAIRE_FILTER_PRIX; break;
          }
          if (query!=null) {
            this.$apollo.query({
              query: query,
              variables: {
                value
              }
            }).then(({data}) => {
              console.log(value)
              let newItineraireInfo=[]
              let newItineraireInfoId=[]
              data.itineraire.forEach(i => {
                if (this.itineraireInfoId.includes(i.id)) {
                  newItineraireInfo.push(i)
                  newItineraireInfoId.push(i.id)
                } else {
                  this.itineraireInfo = newItineraireInfo
                  this.itineraireInfoId = newItineraireInfoId
                  let valid = true
                  this.filters.forEach(filter => {
                    if (filter.key!=f.key && filter.value.length>0 && filter.enabled && filter.value!=null) {
                      valid = false
                    } else {
                      let aQuery = null;
                      let aValue = "%"+filter.value+"%";
                      switch(filter.key) {
                        case 'depart':aQuery=GET_ITINERAIRE_FILTER_DEPART; break;
                        case 'destination':aQuery=GET_ITINERAIRE_FILTER_DESTINATION; break;
                        case 'date':aQuery=GET_ITINERAIRE_FILTER_DATE; break;
                        //case 'prix':aQuery=GET_ITINERAIRE_FILTER_PRIX; break;
                      }
                      if (aQuery!=null) {
                        this.$apollo.query({
                          query: aQuery,
                          variables: {
                            value: aValue
                          }
                        }).then(({data}) => {
                          if (data.itineraire.length==0) {
                            valid = false
                          }
                        }).catch((error) => {
                          console.log(error);
                        });
                      } else { valid = false }
                    }
                  });
                  if (valid) {
                    newItineraireInfo.push(i)
                    newItineraireInfoId.push(i.id)
                  }
                }
              });
              this.itineraireInfo = newItineraireInfo
              this.itineraireInfoId = newItineraireInfoId
              let x = 0;
              for(let itineraire of this.itineraireInfo){
                let s = itineraire.date_depart
                this.itineraireInfo[x].date_depart = s[8]+s[9] +' / '+ s[5]+s[6]+ ' / '+s[0]+s[1]+s[2]+s[3]
                x++;
              }
            }).catch((error) => {
              console.log(error);
            });
          }
        }
      });
    }
  }
}