/* eslint-disable */
import gql from "graphql-tag"
import {Library} from '../../Library'

const GET_USER_BY_ID = gql`
query getUser($id: Int!) {
  utilisateurs(where: {id: {_eq: $id}}) {
    nom
    prenom
    email
    libelle
    marque
    modele
    nb_place
    assurance
    adresse
    composante
    description
    id
    permisConduire
    photo
    telephone
  }
}
`
const GET_ITINERAIRE_BY_USER_ID = gql`
query MyQuery($id: Int!) {
  utilisateurs_itineraire(where: {utilisateur: {_eq: $id}}) {
    conducteur
    itineraire
    itineraireByItineraire {
      depart
      date_depart
      destination
    }
    id
    utilisateur
  }
}
`
const UPDATE_USER_INFO_BY_ID = gql`
mutation updateUserInfo($userId: Int!, $adresse: String!, $description: String!, $email: String!, $nom: String!, $prenom: String!, $telephone: Int!, $password: String!, $composante: String!, $permisConduire: Boolean!) {
  update_utilisateurs_by_pk(_set: {nom: $nom, prenom: $prenom, email: $email, password: $password, telephone: $telephone, adresse: $adresse, description: $description, composante: $composante, permisConduire: $permisConduire}, pk_columns: {id: $userId}) {
    id
    nom
    prenom
    email
    telephone
    adresse
    description
    composante
    permisConduire
  }
}
`
const UPDATE_CAR_BY_USER_ID = gql`
  mutation UpdateCar($id: Int!, $libelle: String!, $marque: String!, $modele: String!, $nb_place: Int!, $assurance: timestamptz) {
  update_utilisateurs(where: {id: {_eq: $id}}, _set: {libelle: $libelle, marque: $marque, modele: $modele, nb_place: $nb_place, assurance: $assurance}) {
    returning {
      libelle
      marque
      modele
      nb_place,
      assurance
    }
  }
}
`
const ADD_CAR = gql`
mutation addCar($id: Int!, $libelle: String!, $marque: String!, $modele: String!, $nb_place: Int!, $assurance: timestamptz!) {
  update_utilisateurs(where: {id: {_eq: $id}}, _set: {libelle: $libelle, marque: $marque, modele: $modele, nb_place: $nb_place, assurance: $assurance}) {
    returning {
      libelle
      marque
      modele
      nb_place,
      assurance
    }
  }
}
`

const DELETE_CAR_USER = gql`
mutation deleteCar($id: Int = 10, $libelle: String = null, $marque: String = null, $modele: String = null, $nb_place: Int = null, $assurance: timestamptz = null) {
  update_utilisateurs(where: {id: {_eq: $id}}, _set: {libelle: $libelle, marque: $marque, modele: $modele, nb_place: $nb_place, assurance: $assurance}) {
    returning {
      libelle
      marque
      modele
      nb_place,
      assurance
    }
  }
}
`

export default {
  data() {
    return {
      userInfo: [],
      itineraireInfo: [],
      id: parseInt(localStorage.getItem('userId')),
      param: parseInt(this.$route.params.userId),
      hasCar: null,
      voitureId: null,
      loading: false,
      adminOnPage: false,
      lang: 'fr'
    }
  },
  apollo: {},
  mounted() {
    this.lang = window.localStorage.getItem('lang')

    if (!this.id){
      return this.$router.push({name: 'logInForm'})
    }
    if (this.id === this.param) {
      this.adminOnPage = true
    }

    //localStorage.setItem('paramUserId', this.param);

    const id = this.$data.param
    this.$apollo.query({
      query: GET_USER_BY_ID,
      variables: {
        id
      }
    }).then(({data}) => {
      this.userInfo = data.utilisateurs[0];
      console.log(this.userInfo)
      if (this.userInfo.libelle !== null) {
        this.hasCar = true
      } else {
        this.hasCar = false
      }
      let s = this.userInfo.assurance
      this.userInfo.assurance = s[8]+s[9]+' / '+s[5]+s[6]+' / '+s[0]+s[1]+s[2]+s[3]
    }).catch((error) => {
      console.log(error);
    });

    this.displayUserInfo(id)
  },
  methods: {
    getDay (i) {
      return this.itineraireInfo[i].itineraireByItineraire.date_depart.substring(0,10)
    },
    getHours (i) {
      return this.itineraireInfo[i].itineraireByItineraire.date_depart.substring(11,16)
    },
    getCarInputs () {
      return document.querySelector('#carPopUp').querySelectorAll('input')
    },
    getUserInputs () {
      return document.querySelector('#userPopUp').querySelectorAll('input')
    },
    convertIsConducteur (i) {
      if (this.itineraireInfo[i].conducteur === true) {
        return this.itineraireInfo[i].conducteur = 'oui'
      } else {
        return this.itineraireInfo[i].conducteur = 'non'
      }
    },
    displayUserInfo (id) {
      this.$apollo.query({
        query: GET_ITINERAIRE_BY_USER_ID,
        variables: {
          id
        }
      }).then(({data}) => {
        let resp = data.utilisateurs_itineraire
        for (let i = 0; i < resp.length; i++) {
          this.itineraireInfo.push(resp[i])
          this.itineraireInfo[i].itineraireByItineraire.date_depart = this.getDay(i) + ' à ' + this.getHours(i)
          this.itineraireInfo[i].conducteur = this.convertIsConducteur(i)
        }
      }).catch((error) => {
        console.log(error);
      });
    },
    modifUser () {
      if (this.adminOnPage===true) {
        document.querySelector('#userPopUp').style.display = 'block'
        let inputs = document.querySelector('#userPopUp').querySelectorAll('input')
        inputs.forEach(element => {
          switch (element.id) {
            case 'nom':
              element.value = this.userInfo.nom
              break
            case 'prenom':
              element.value = this.userInfo.prenom
              break
            case 'email':
              element.value = this.userInfo.email
              break
            case 'telephone':
              element.value = this.userInfo.telephone
              break
            case 'adresse':
              element.value = this.userInfo.adresse
              break
            case 'description':
              element.value = this.userInfo.description
              break
          }
        })
        document.querySelector('#userPopUp').querySelector('textarea').value = this.userInfo.description
        document.querySelector('#userPopUp').querySelector('select').value = this.userInfo.composante
      }
    },
    modifVoiture () {
      if (this.adminOnPage===true) {
        document.querySelector('#carPopUp').style.display = 'block'
        let inputs = this.getCarInputs()

        if (this.userInfo.libelle === null) {
          return false
        }

        inputs.forEach(element => {
          switch (element.id) {
            case 'nomVoiture':
              element.value = this.userInfo.libelle
              break
            case 'marque':
              element.value = this.userInfo.marque
              break
            case 'modele':
              element.value = this.userInfo.modele
              break
            case 'nbPlace':
              element.value = this.userInfo.nb_place
              break
          }
        })
      }
    },
    closeUserPopup: function (e) {
      document.querySelector('#userPopUp').style.display = 'none'
    },
    closeVoiturePopup: function (e) {
      document.querySelector('#carPopUp').style.display = 'none'
    },
    submitModifUser: function (e) {
      if (this.adminOnPage===true) {
        this.loading = true
        let inputs = this.getUserInputs()

        this.userInfo.nom = inputs[0].value
        this.userInfo.prenom = inputs[1].value
        this.userInfo.password = inputs[2].value
        this.userInfo.email = inputs[3].value
        this.userInfo.telephone = inputs[4].value
        this.userInfo.adresse = inputs[5].value
        this.userInfo.description = document.querySelector('#userPopUp').querySelector('textarea').value
        this.userInfo.composante = document.querySelector('#userPopUp').querySelector('select').value
        if (document.getElementsByName('driverLicence')[0].checked)
          this.userInfo.permisConduire = true
        else
          this.userInfo.permisConduire = false

        let nom  = this.userInfo.nom
        let prenom = this.userInfo.prenom
        let password = this.userInfo.password
        let email = this.userInfo.email
        let telephone = parseInt(this.userInfo.telephone)
        let adresse = this.userInfo.adresse
        let description = this.userInfo.description
        let composante = this.userInfo.composante
        let permisConduire = this.userInfo.permisConduire
        let userId = this.param
        Library.hashPassword(password).then(r => {
          password = r
          this.$apollo.mutate({
            mutation: UPDATE_USER_INFO_BY_ID,
            variables: {
              userId,
              nom,
              prenom,
              password,
              telephone,
              email,
              adresse,
              description,
              composante,
              permisConduire
            }
          }).then(r => {
            let resp = r.data.update_utilisateurs_by_pk
            this.userInfo.nom = resp.nom
            this.userInfo.prenom = resp.prenom
            this.userInfo.telephone = resp.telephone
            this.userInfo.email = resp.email
            this.userInfo.adresse = resp.adresse
            this.userInfo.description = resp.description
            this.userInfo.composante = resp.composante
            this.userInfo.permisConduire = resp.permisConduire
            this.loading = false
            this.closeUserPopup()
          }).catch((error) => {
            console.log(error)
          });
        })
      }
    },
    addVoiture () {
      if (this.adminOnPage===true) {
        this.loading = true
        let inputs = this.getCarInputs()
        this.userInfo.libelle = inputs[0].value
        this.userInfo.marque = inputs[1].value
        this.userInfo.modele = inputs[2].value
        this.userInfo.nb_place = inputs[3].value
        this.userInfo.assurance = inputs[4].value

        let id = this.param
        let libelle  = this.userInfo.libelle
        let marque = this.userInfo.marque
        let modele = this.userInfo.modele
        let nb_place = parseInt(this.userInfo.nb_place)
        let assurance = this.userInfo.assurance
        this.$apollo.mutate({
          mutation: ADD_CAR,
          variables: {
            id,
            libelle,
            marque,
            modele,
            nb_place,
            assurance
          }
        }).then(r => {
          let resp = r.data.update_utilisateurs
          this.userInfo.libelle = resp.returning[0].libelle
          this.userInfo.marque = resp.returning[0].marque
          this.userInfo.modele = resp.returning[0].modele
          this.userInfo.nb_place = resp.returning[0].nb_place
          this.userInfo.assurance = Library.transform_date(resp.returning[0].assurance)

          this.hasCar = true
          this.loading = false
          this.closeVoiturePopup()
        }).catch((error) => {
          console.log(error);
        });
      }
    },
    sumbitModifVoiture () {
      if (this.adminOnPage===true) {
        this.loading = true
        let inputs = this.getCarInputs()
        this.userInfo.libelle = inputs[0].value
        this.userInfo.marque = inputs[1].value
        this.userInfo.modele = inputs[2].value
        this.userInfo.nb_place = inputs[3].value
        this.userInfo.assurance = inputs[4].value

        let id = this.param
        let libelle  = this.userInfo.libelle
        let marque = this.userInfo.marque
        let modele = this.userInfo.modele
        let nb_place = parseInt(this.userInfo.nb_place)
        let assurance = this.userInfo.assurance
        this.$apollo.mutate({
          mutation: UPDATE_CAR_BY_USER_ID,
          variables: {
            id,
            libelle,
            marque,
            modele,
            nb_place,
            assurance
          }
        }).then(r => {
          let updatedCar = r.data.update_utilisateurs
          this.userInfo.libelle = updatedCar.returning[0].libelle
          this.userInfo.marque = updatedCar.returning[0].marque
          this.userInfo.modele = updatedCar.returning[0].modele
          this.userInfo.nb_place = updatedCar.returning[0].nb_place
          this.userInfo.assurance = Library.transform_date(updatedCar.returning[0].assurance)
          this.loading = false
          this.closeVoiturePopup()
        }).catch((error) => {
          console.log(error);
        });
      }
    },
    deleteVoiture () {
      if (this.adminOnPage===true) {
        const res = window.confirm('Voulez vous vraiment supprimer cette voiture ?')
        if (res === false) {
          return
        }
        const id = this.param
        this.$apollo.mutate({
          mutation: DELETE_CAR_USER,
          variables: {
            id
          }
        }).then(r => {
          let resp = r.data.update_utilisateurs
          this.userInfo.libelle = null
          this.userInfo.marque = null
          this.userInfo.modele = null
          this.userInfo.nb_place = null
          this.userInfo.assurance = null

          this.hasCar = false
        })
      }
    }
  },
  watch: {
    lang: function() {
      window.localStorage.setItem('lang', this.lang)
      translateAllElements(this.lang);
    }
  }
}

// translation //
let locale = window.localStorage.getItem('lang');

const translations = {
  // francais
  "fr": {
    "nav-home": "Accueil",
    "nav-route-proposal": "Proposer un trajet",
    "nav-routes-list": "Liste des itinéraires",
    "nav-login": "Connexion",
    "nav-account": "Compte",
    "nav-account-details": "Détails",
    "nav-logout": "Déconnexion",
    "nav-full-logout": "Déco totale",
    "modify-infos-title": "Modifier vos informations",
    "modify-infos-input-name": "Nom",
    "modify-infos-input-firstname": "Prénom",
    "modify-infos-input-password": "Mot de passe",
    "modify-infos-input-phone": "Téléphone",
    "modify-infos-input-address": "Adresse",
    "modify-infos-input-driver-licence": "Permis de conduire",
    "user-infos-driver-licence-yes": "Oui",
    "user-infos-driver-licence-no": "Non",
    "modify-infos-car-title": "Modifier vos informations",
    "modify-infos-car-add-car-title": "Ajouter une voiture",
    "infos-user-car-name": "Nom",
    "popup-submit": "Valider",
    "popup-close": "Fermer",
    "infos-user-title": "Infos utilisateur",
    "infos-user-name": "Nom",
    "infos-user-firstname": "Prénom",
    "infos-user-phone": "Téléphone",
    "infos-user-address": "Adresse",
    "infos-user-driver-licence": "Permis",
    "infos-user-component": "Composante",
    "modify": "Modifier",
    "infos-user-car-title": "Infos voiture",
    "infos-user-car-brand": "Marque",
    "infos-user-car-model": "Modèle",
    "infos-user-car-seats-number": "Nombre de places",
    "infos-user-car-insurance-end-date": "Date fin de contrat d'assurance",
    "infos-user-car-no-car": "Aucune voiture renseignée.",
    "add": "Ajouter",
    "remove": "Retirer",
    "infos-user-routes-title": "Présence sur les itinéraires :",
    "infos-user-routes-departure": "Départ",
    "infos-user-routes-date": "Date de départ",
    "infos-user-routes-driver": "Conducteur",
    "infos-user-routes-details": "Détails",
    "infos-user-routes-no-route": "Aucune présence sur un itinéraire.",
    "email-not-univ": "Votre compte d'authentification n'appartient pas à l'université d'Orléans.",
    "email-not-univ-auth": "Authentification",
    "email-not-univ-full-logout": "Déconnexion totale"
  },

  // anglais
  "en": {
    "nav-home": "Home",
    "nav-route-proposal": "Propose a route",
    "nav-routes-list": "Routes list",
    "nav-login": "Login",
    "nav-account": "Account",
    "nav-account-details": "Detail",
    "nav-logout": "Logout",
    "nav-full-logout": "Full logout",
    "modify-infos-title": "Modify your informations",
    "modify-infos-input-name": "Name",
    "modify-infos-input-firstname": "First name",
    "modify-infos-input-password": "Password",
    "modify-infos-input-phone": "Phone number",
    "modify-infos-input-address": "Address",
    "modify-infos-input-driver-licence": "Driver licence",
    "user-infos-driver-licence-yes": "Yes",
    "user-infos-driver-licence-no": "No",
    "modify-infos-car-title": "Modify your informations",
    "modify-infos-car-add-car-title": "Add a car",
    "infos-user-car-name": "Name",
    "popup-submit": "Submit",
    "popup-close": "Close",
    "infos-user-title": "User informations",
    "infos-user-name": "Name",
    "infos-user-firstname": "First name",
    "infos-user-phone": "Phone number",
    "infos-user-address": "Address",
    "infos-user-driver-licence": "Driver licence",
    "infos-user-component": "Component",
    "modify": "Modify",
    "infos-user-car-title": "Car informations",
    "infos-user-car-brand": "Brand",
    "infos-user-car-model": "Model",
    "infos-user-car-seats-number": "Seats number",
    "infos-user-car-insurance-end-date": "Insurance contract end date",
    "infos-user-car-no-car": "No car specified.",
    "add": "Add",
    "remove": "Remove",
    "infos-user-routes-title": "Presence on routes :",
    "infos-user-routes-departure": "Departure",
    "infos-user-routes-date": "Departure date",
    "infos-user-routes-driver": "Driver",
    "infos-user-routes-details": "Details",
    "infos-user-routes-no-route": "No presence on a route.",
    "email-not-univ": "Your authentication account does not belong to the Orléans University.",
    "email-not-univ-auth": "Authentication",
    "email-not-univ-full-logout": "Full logout"
  },
};

document.addEventListener("DOMContentLoaded", () => {
  translateAllElements(locale);
});

function translateAllElements(lang) {
  translateElementTextTrigger(lang);
  translateElementPlaceholderTrigger(lang);
}

// texts
function translateElementTextTrigger(lang) {
  document.querySelectorAll("[data-i18n-key]").forEach(elem => {
    translateElementText(elem, lang)
  });
}
function translateElementText(element, lang) {
  const key = element.getAttribute("data-i18n-key");
  const translation = translations[lang][key];
  if (translations[lang][key]!=null)
    element.innerText = translation;
}

// placeholders
function translateElementPlaceholderTrigger(lang) {
  document.querySelectorAll("[data-i18n-placeholder-key]").forEach(elem => {
    translateElementPlaceholder(elem, lang)
  });
}
function translateElementPlaceholder(element, lang) {
  const key = element.getAttribute("data-i18n-placeholder-key");
  const translation = translations[lang][key];
  if (translations[lang][key]!=null)
    element.placeholder = translation;
}
