import gql from "graphql-tag";
import {Library} from '../../Library'
import Rating from '../Rating/Rating'

// Requêtes GraphQL à l'API
const GET_ITINERAIRE = gql`
query get_itineraire ($id: Int!) {
  itineraire_by_pk(id: $id) {
    date_depart
    depart
    description
    destination
    id
    nb_passager
    prix
    temps_trajet
    point_depart
    point_arrivee
  }
}
`;

const GET_UTILISATEUR_ITINERAIRE = gql`
query get_utilisateur_itineraire ($id: Int!) {
  utilisateurs_itineraire(where: {itineraireByItineraire: {id: {_eq: $id}}}) {
    id
    conducteur
    utilisateurByUtilisateur {
      id
      nom
      prenom
      telephone
      description
      composante
      libelle
      marque
      modele
      nb_place

    }
  }
}
`;

const GET_NOTES_CONDUCTEUR_ITINERAIRE = gql`
query get_notes_conducteur_itineraire ($idConducteur: Int!, $idItineraire: Int!) {
  notation(where: {utilisateurConducteur: {_eq: $idConducteur}, itineraire: {_eq: $idItineraire}}) {
    note
    utilisateurPassager
    itineraire
  }
}
`;

const ADD_UTILISATEUR_ITINERAIRE = gql`
mutation add_utilisateur_itineraire($idItineraire: Int!, $idUtilisateur: Int!) {
  insert_utilisateurs_itineraire(objects: {itineraire: $idItineraire, utilisateur: $idUtilisateur, conducteur: false}) {
    returning {
      id
      utilisateurByUtilisateur {
        id
        nom
        prenom
        telephone
      }
    }
  }
}
`;
const SUPPR_UTILISATEUR_ITINERAIRE = gql`
mutation supp_utilisateur_itineraire($idItineraire: Int!, $idUtilisateur: Int!) {
  delete_utilisateurs_itineraire(where: {itineraire: {_eq: $idItineraire}, utilisateur: {_eq: $idUtilisateur}, _and: {conducteur: {_eq: false}}}) {
    returning {
      id
    }
  }
}
`;

const UPDATE_ITINERAIRE = gql`
mutation UpdateItineraire($id: Int! ,$date_depart: timestamptz!, $depart: String!, $destination: String!, $nb_passager: Int!, $point_arrivee: point!, $point_depart: point!, $prix: float8!, $temps_trajet: float8!) {
  update_itineraire_by_pk(pk_columns: {id: $id}, _set: {date_depart: $date_depart, depart: $depart, destination: $destination, nb_passager: $nb_passager, point_arrivee: $point_arrivee, point_depart: $point_depart, prix: $prix, temps_trajet: $temps_trajet}) {
    date_depart
    depart
    destination
    nb_passager
    point_arrivee
    point_depart
    prix
    temps_trajet
  }
}
`;

const DELETE_ITINERAIRE = gql`
mutation delete_itineraire($id: Int!) {
  delete_utilisateurs_itineraire(where: {itineraire: {_eq : $id}}) {
    affected_rows
  }
  delete_itineraire_by_pk(id: $id) {
    id
  }
}
`;

const INSERT_NOTES = gql`
mutation insertNote($itineraire: Int!, $note: Int!, $utilisateurConducteur: Int!, $utilisateurPassager: Int!) {
  insert_notation(objects: [{itineraire: $itineraire, note: $note, utilisateurConducteur: $utilisateurConducteur, utilisateurPassager: $utilisateurPassager}]) {
    affected_rows
  }
}
`;



const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' };
var temps_trajet = 0;
var point_depart = null;
var point_arrivee = null;
export default {
  name: 'detailsItinerairePage',
  data () {
    return {
      id: parseInt(this.$route.params.id),
      Itineraire: null,
      utilisateur_connecter: parseInt(window.localStorage.getItem('userId')),
      utilisateurs_itineraire: null,
      date: null,
      duree: null,
      notes: null,
      notesString: null,
      conducteur: null,
      point_depart: null,
      point_arrivee: null,
      isIn: false,
      isUpdate: false,
      isDelete: false,
      date_depart: null,
      prix: null,
      nb_passager: null,
      depart: null,
      destination: null,
      temps_trajet: null,
      passagers: [],
      stars: null,
      hasNote: false,
      loading: 0,
      nbtrajets: 0,
      lang: 'fr'
    }
  },
  apollo: {},
  // chargement du composant pour les notes
  components: { Rating },
  filters: {
    get_first_letter: function (value) {
      return value.slice(0, 1) + "."
    }
  },
  methods: {
    // Lorsque un utilisateur connecté clique sur le bouton pour participer à un trajet
    participerTrajet: function (event) {
      var idItineraire = this.Itineraire.id
      var idUtilisateur = this.utilisateur_connecter
      this.$apollo.mutate({
        mutation: ADD_UTILISATEUR_ITINERAIRE,
        variables: {
          idItineraire,
          idUtilisateur
        }
      }).then(({data}) => {
        this.passagers.push(data.insert_utilisateurs_itineraire.returning[0].utilisateurByUtilisateur);
        this.isIn = true
      }).catch((error) => {
        console.log(error);
      });
    },
    // Lorsque un utilisateur connecté participant à un trajet clique sur le bouton pour quitter ce trajet
    quitterTrajet: function (event) {
      var idItineraire = this.Itineraire.id
      var idUtilisateur = this.utilisateur_connecter
      this.$apollo.mutate({
        mutation: SUPPR_UTILISATEUR_ITINERAIRE,
        variables: {
          idItineraire,
          idUtilisateur
        }
      }).then(({data}) => {
        this.passagers.forEach(u => {
          if (u.id === this.utilisateur_connecter) {
            this.passagers.splice(this.passagers.indexOf(u),1)
          }
        });
        this.isIn = false
      }).catch((error) => {
        console.log(error);
      });
    },
    // Lorsque l'utilisateur clique sur le bouton pour modifier un trajet
    toggleModalUpdate: function(event){
      this.isUpdate=!this.isUpdate
      this.isDelete=false
    },
    // Lorsque l'utilisateur clique sur le bouton pour supprimer un trajet
    toggleModalDelete: function(event){
      this.isDelete=!this.isDelete
      this.isUpdate=false
    },
    toggleModalUpdateCancel: function(event){
      this.isUpdate=false
    },
    toggleModalDeleteCancel: function(event){
      this.isDelete=false
    },
    // Lorsque un utilisateur valide le formulaire de modification d'un trajet
    submitUpdate: function (e) {

      if (!localStorage.getItem('userId')){
        return this.$router.push({name: 'logInForm'})
      }
      this.id = this.Itineraire.id
      this.date_depart = new Date($('#date_depart')[0].value)
      this.temps_trajet = temps_trajet
      this.depart = $('#mapid input')[0].value
      this.destination = $('#mapid input')[$('#mapid input').length - 1].value
      this.point_depart = point_depart
      this.point_arrivee = point_arrivee
      this.nb_passager = parseInt($('#nb_passager')[0].value)
      this.prix = parseInt($('#prix')[0].value)

      if ($('#date_depart')[0].value !== '' && this.nb_passager && this.prix && $('.leaflet-marker-icon').length >= 2) {
        const { id, depart, destination, date_depart, nb_passager, point_depart, point_arrivee, prix, temps_trajet} = this.$data;
        this.$apollo.mutate({
          mutation: UPDATE_ITINERAIRE,
          variables: {
            id,
            date_depart,
            depart,
            destination,
            nb_passager,
            point_depart,
            point_arrivee,
            prix,
            temps_trajet
          }
        }).then(({data}) => {
          window.location.reload()
        }).catch((error) => {
          console.log(error);
        });
      }

      this.errors = []

      if ($('.leaflet-marker-icon').length < 2) {
        this.errors.push('Choisissez un point de départ et un point d\'arrivée')
      }
      if ($('#date_depart')[0].value === '') {
        this.errors.push('Choisissez une date et une heure de départ')
      }
      if (!this.nb_passager) {
        this.errors.push('Choisissez le nombre de passagers possible')
      }
      if (!this.prix) {
        this.errors.push('Choisissez le prix par passager')
      }

      e.preventDefault()
    },
    // Lorsque l'utilisateur veut supprimer l'itineraire
    submitDelete: function(e) {
      let id = this.id
      this.$apollo.mutate({
        mutation: DELETE_ITINERAIRE,
        variables: {
          id
        }
      }).then(({data}) => {
        this.$router.push('/');
      }).catch((error) => {
        console.log(error);
      });
      e.preventDefault()
    },
    // Lorsque un utilisateur valide le formulaire de notation d'un conducteur
    note: function(e){
      var itineraire = this.Itineraire.id
      var utilisateurPassager = this.utilisateur_connecter
      var utilisateurConducteur = this.conducteur.id
      let note = document.querySelectorAll('#notes')[0].outerText
      note = parseInt(note.substring(0,1))
      this.$apollo.mutate({
        mutation: INSERT_NOTES,
        variables: {
          itineraire,
          note,
          utilisateurConducteur,
          utilisateurPassager
        }
      }).then(res =>{
        this.notes = tot / data.notation.length
        this.notesString = `${(this.notes + note)/(this.nbtrajets+1)}/5`
        this.hasNote = true
      }).catch((error) => {
        console.log(error);
      });
      e.preventDefault()
    }
  },
  mounted () {

    console.log(this.lang)
    this.lang = window.localStorage.getItem('lang')

    if (!localStorage.getItem('userId')){
      return this.$router.push({name: 'logInForm'})
    }

    const { id } = this.$data;

    // recuperation des détails de l'itinéraire
    this.$apollo.query({
      query: GET_ITINERAIRE,
      variables: {
        id
      }
    }).then(({data}) => {
      this.Itineraire = data.itineraire_by_pk;

      this.date = Library.transform_date_heure(data.itineraire_by_pk.date_depart)
      this.duree = Library.get_duree_from_sec(data.itineraire_by_pk.temps_trajet)

      this.point_depart = JSON.parse(this.Itineraire.point_depart.replace('(', '[').replace(')', ']'))
      this.point_arrivee = JSON.parse(this.Itineraire.point_arrivee.replace('(', '[').replace(')', ']'))
      this.loading += 1;
    }).catch((error) => {
      console.log(error);
      this.$router.push({name: 'WelcomePage'})
      alert('Cet itinéraire n\'éxiste pas')
    });

    // recuperation des utilisateurs participant a ce trajet
    this.$apollo.query({
      query: GET_UTILISATEUR_ITINERAIRE,
      variables: {
        id
      }
    }).then(({data}) => {
      this.utilisateurs_itineraire = data.utilisateurs_itineraire;
      data.utilisateurs_itineraire.forEach(u => {
        if (u.conducteur) {
          this.conducteur = u.utilisateurByUtilisateur;

          // set de l'id du conducteur et de l'itineraire
          const idConducteur = this.conducteur.id;
          const idItineraire = this.Itineraire.id;
          // recuperation des notes du conducteur pour cet itineraire
          this.$apollo.query({
            query: GET_NOTES_CONDUCTEUR_ITINERAIRE,
            variables: {
              idConducteur,
              idItineraire
            }
          }).then(({data}) => {
            let tot = 0;
            let idUtilisateur = this.utilisateur_connecter
            data.notation.forEach(n => {
              tot += n.note
              if(n.utilisateurPassager==idUtilisateur && n.itineraire==this.Itineraire.id){
                this.hasNote=true
              }
            });
            if (data.notation.length > 0) {
              this.notes = tot / data.notation.length
              this.notes = this.notes.toFixed(1)
              if (this.notes==parseInt(this.notes.toString(), 10))
                this.notes = parseInt(this.notes.toString(), 10)
              this.notesString = `${this.notes}/5`
              this.nbtrajets = data.notation.length
            }else{
              this.notes = ""
            }
          }).catch((error) => {
            console.log(error);
          });

        }else{
          this.passagers.push(u.utilisateurByUtilisateur);
          if (u.utilisateurByUtilisateur.id === this.utilisateur_connecter) {
            this.isIn = true;
          }
        }
      });

      this.loading += 1;

    }).catch((error) => {
      console.log(error);
    });
  },
  updated: function () {
    if (this.loading === 2 && $('#mapid')[0].innerHTML === "" && this.isUpdate == false && this.isDelete == false) {
      var L = window.L
      var mymap = L.map('mapid')

      L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Itineraire du trajet',
        maxZoom: 25,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'sk.eyJ1IjoibWF0dGhpZXU0OSIsImEiOiJja2t4djM4dHEwM2kzMnBxb3czam9vdXg2In0.j6qvI5SY3BNQVCUMuN4a7w'
      }).addTo(mymap)

      const control = L.Routing.control({
        draggableWaypoints: false,
        addWaypoints: false,
        language: this.lang,
        routeWhileDragging: false,
        waypoints: [
          L.latLng(this.point_depart[0], this.point_depart[1]),
          L.latLng(this.point_arrivee[0], this.point_arrivee[1]),
        ],
        lineOptions: {
          styles: [{
            color: '#791866',
            opacity: 0.8,
            weight: 4
          }]
        },
        pointMarkerStyle: {
          radius: 5,
          color: '#791866',
          fillOpacity: 1,
          fillColor: '#767375',
        },
        router: new L.Routing.osrmv1({
          language: this.lang
        })
      }).addTo(mymap)
    }
    else if (this.loading === 2 && $('#mapid')[0].innerHTML === "" && this.isUpdate == true && this.isDelete == false) {
      var L = window.L
      var mymap = L.map('mapid').setView([47.843195, 1.926056], 17)

      L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Itineraire du trajet',
        maxZoom: 25,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'sk.eyJ1IjoibWF0dGhpZXU0OSIsImEiOiJja2t4djM4dHEwM2kzMnBxb3czam9vdXg2In0.j6qvI5SY3BNQVCUMuN4a7w'
      }).addTo(mymap)

      const control = L.Routing.control({
        language: this.lang,
        routeWhileDragging: false,
        geocoder: L.Control.Geocoder.nominatim(),
        lineOptions: {
          styles: [{
            color: '#791866',
            opacity: 0.8,
            weight: 4
          }]
        },
        pointMarkerStyle: {
          radius: 5,
          color: '#791866',
          fillOpacity: 1,
          fillColor: '#767375',
        },
        router: new L.Routing.osrmv1({
          language: this.lang
        })
      }).addTo(mymap)
      control.on('routeselected', function (e) {
        point_depart = `${control.getWaypoints()[0].latLng.lat}, ${control.getWaypoints()[0].latLng.lng}`;
        point_arrivee = `${control.getWaypoints()[control.getWaypoints().length - 1].latLng.lat}, ${control.getWaypoints()[control.getWaypoints().length - 1].latLng.lng}`;
        temps_trajet = control._routes[e.route.routesIndex].summary.totalTime;

      }.bind(this));
    }
  },
  watch: {
    lang: function() {
      window.localStorage.setItem('lang', this.lang)
      translateAllElements(this.lang);
    }
  }
}

// translation //
let locale = window.localStorage.getItem('lang');

const translations = {
  // francais
  "fr": {
    "nav-home": "Accueil",
    "nav-route-proposal": "Proposer un trajet",
    "nav-routes-list": "Liste des itinéraires",
    "nav-login": "Connexion",
    "nav-account": "Compte",
    "nav-account-details": "Détails",
    "nav-logout": "Déconnexion",
    "nav-full-logout": "Déco totale",
    "route-modify": "Modifier",
    "route-modify-cancel": "(annuler)",
    "route-delete": "Supprimer",
    "route-delete-cancel": "(annuler)",
    "route-price-text": " euros par passager",
    "route-passengers-text": " passager(s) possible",
    "route-driver-text": "Proposé par :",
    "route-driver-details": "Détails",
    "route-driver-notation-submit": "Valider",
    "route-driver-phone-text": "Téléphone :",
    "route-driver-car-text": "Véhicule :",
    "route-driver-no-car": "Aucun véhicule renseigné.",
    "route-driver-descr-text": "Description :",
    "route-no-driver": "Aucun conducteur pour ce trajet.",
    "route-passengers-list-text": "Liste des passagers",
    "route-passenger-participate": "Participer au trajet",
    "route-passenger-leave": "Se retirer du trajet",
    "page-title-modify-route": "Modifier le trajet",
    "modify-route-passengers-number": "Nombre de passagers",
    "modify-route-price": "Prix",
    "modify-route-submit": "Valider",
    "page-title-delete-route": "Supprimer le trajet",
    "delete-route-confirm": "Confirmer la suppression du trajet ?",
    "delete-route-delete": "Supprimer",
    "delete-route-cancel": "Annuler",
    "email-not-univ": "Votre compte d'authentification n'appartient pas à l'université d'Orléans.",
    "email-not-univ-login": "Authentification",
    "email-not-univ-full-logout": "Déconnexion totale"
  },

  // anglais
  "en": {
    "nav-home": "Home",
    "nav-route-proposal": "Propose a route",
    "nav-routes-list": "Routes list",
    "nav-login": "Login",
    "nav-account": "Account",
    "nav-account-details": "Detail",
    "nav-logout": "Logout",
    "nav-full-logout": "Full logout",
    "route-modify": "Modify",
    "route-modify-cancel": "(cancel)",
    "route-delete": "Delete",
    "route-delete-cancel": "(cancel)",
    "route-price-text": " euros per passenger",
    "route-passengers-text": " passenger(s) possible",
    "route-driver-text": "Proposed by :",
    "route-driver-details": "Details",
    "route-driver-notation-submit": "Submit",
    "route-driver-phone-text": "Phone number :",
    "route-driver-car-text": "Vehicle :",
    "route-driver-no-car": "No vehicle specified.",
    "route-driver-descr-text": "Description :",
    "route-no-driver": "No driver for this route.",
    "route-passengers-list-text": "Passengers list",
    "route-passenger-participate": "Join route",
    "route-passenger-leave": "Leave route",
    "page-title-modify-route": "Modify route",
    "modify-route-passengers-number": "Passengers number",
    "modify-route-price": "Price",
    "modify-route-submit": "Submit",
    "page-title-delete-route": "Delete route",
    "delete-route-confirm": "Confirm delete route ?",
    "delete-route-delete": "Delete",
    "delete-route-cancel": "Cancel",
    "email-not-univ": "Your authentication account does not belong to the Orléans University.",
    "email-not-univ-auth": "Authentication",
    "email-not-univ-full-logout": "Full logout"
  },
};

document.addEventListener("DOMContentLoaded", () => {
  translateAllElements(locale);
});

function translateAllElements(lang) {
  translateElementTextTrigger(lang);
  translateElementPlaceholderTrigger(lang);
  translateElementValueTrigger(lang);
}

// texts
function translateElementTextTrigger(lang) {
  document.querySelectorAll("[data-i18n-key]").forEach(elem => {
    translateElementText(elem, lang)
  });
}
function translateElementText(element, lang) {
  const key = element.getAttribute("data-i18n-key");
  const translation = translations[lang][key];
  if (translations[lang][key]!=null)
    element.innerText = translation;
}

// placeholders
function translateElementPlaceholderTrigger(lang) {
  document.querySelectorAll("[data-i18n-placeholder-key]").forEach(elem => {
    translateElementPlaceholder(elem, lang)
  });
}
function translateElementPlaceholder(element, lang) {
  const key = element.getAttribute("data-i18n-placeholder-key");
  const translation = translations[lang][key];
  if (translations[lang][key]!=null)
    element.placeholder = translation;
}

// values
function translateElementValueTrigger(lang) {
  document.querySelectorAll("[data-i18n-value-key]").forEach(elem => {
    translateElementValue(elem, lang)
  });
}
function translateElementValue(element, lang) {
  const key = element.getAttribute("data-i18n-value-key");
  const translation = translations[lang][key];
  if (translations[lang][key]!=null)
    element.value = translation;
}
