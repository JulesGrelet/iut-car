import gql from "graphql-tag"
import userList from '@/components/usersListPage/usersListPage'

const GET_NOM_PRENOM_USER = gql `
  query getNomPrenom($userId: Int!) {
  utilisateurs(where: {id: {_eq: $userId}}) {
    nom
    prenom
  }
}
`;

export default {
    mounted() {
        this.lang = window.localStorage.getItem('lang')
        this.isAuth()
        if (!isNaN(this.$data.userId))
            this.getNomPrenomUser(this.$data.userId)
    },
    data() {
        return {
            isAuthenticated: false,
            userId: parseInt(localStorage.getItem('userId')),
            lastName: '',
            firstName: '',
            userQuery: '',
            show: false,
            lang: 'fr'
        }
    },
    methods: {

        disconnect: function() {
            localStorage.removeItem('userId')
            if (this.$router.history.current.path !== '/') {
                this.$router.push({ name: 'WelcomePage' })
            }
            location.reload()
        },
        connect: function () {
            this.$router.push({name: 'logInForm'})
        },
        isAuth: function() {
            if (localStorage.getItem('userId')) {
                this.isAuthenticated = true
            } else {
                this.isAuthenticated = false
            }
        },
        getNomPrenomUser: function(userId) {
            this.$apollo.query({
                query: GET_NOM_PRENOM_USER,
                variables: {
                    userId
                }
            }).then(({ data }) => {
                this.lastName = data.utilisateurs[0].nom
                this.firstName = data.utilisateurs[0].prenom
            }).catch((error) => {
                console.error(error)
            })
        },
        login: function() {
            this.$auth.loginWithRedirect();
        },
        logout: function() {
            this.$auth.logout({
                returnTo: window.location.origin
            })
        }
    },
    components: {
        'userList': userList
    }
}
