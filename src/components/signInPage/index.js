import gql from "graphql-tag"

const bcryptjs = require('bcryptjs')

const ADD_USER = gql`
  mutation addUser(
    $lastName: String!
    $firstName: String!
    $password: String!
    $email: String!
    $phone: Int!
    $address: String!
    $driverLicence: Boolean!
    $composante: String!
  ) {
    insert_utilisateurs(
      objects: [
        {
          nom: $lastName
          prenom: $firstName
          password: $password
          email: $email
          telephone: $phone
          adresse: $address
          permisConduire: $driverLicence
          composante: $composante
        }
      ]
    ){
      returning {
        id
      }
    }
  }
`;

export default {
  name: 'signInForm',
  data() {
    return {
      errors: [],
      idUser: null,
      lastName: null,
      firstName: null,
      password: null,
      email: null,
      phone: null,
      address: null,
      driverLicence: null,
      composante: null,
      lang: 'fr'
    }
  },
  mounted() {
    this.lang = window.localStorage.getItem('lang')
  },
  apollo: {},
  methods: {
    checkForm: function (e) {
      this.errors = [];

      if (!this.lastName) {
        this.errors.push('Nom obligatoire')
      }
      if (!this.firstName) {
        this.errors.push('Prénom obligatoire')
      }
      if (!this.password) {
        this.errors.push('Mot de passe obligatoire')
      }
      if (!this.email) {
        this.errors.push('Email obligatoire')
      }
      if (!this.phone) {
        this.errors.push('Téléphone obligatoire')
      }
      if (!this.address) {
        this.errors.push('Adresse obligatoire')
      }
      if (!this.driverLicence || (this.driverLicence!="y" && this.driverLicence!="n")) {
        this.errors.push('Permis de conduire : cocher oui ou non')
      }

      e.preventDefault()

      if (this.errors.length==0) {
        this.changeType()
        this.hashPassword().then(r => this.signInAction())
        this.$router.push({name: 'WelcomePage'})
      }
    },
    signInAction: function () {
      const { errors, lastName, firstName, password, email, address, phone, driverLicence, composante } = this.$data
      this.$apollo.mutate({
        mutation: ADD_USER,
        variables: {
          lastName,
          firstName,
          password,
          email,
          address,
          phone,
          driverLicence,
          composante
        },
      }).then(resp => {
        this.idUser = resp.data.insert_utilisateurs.returning[0].id
        localStorage.setItem('userId', this.idUser);
        location.reload()
      })
    },
    changeType() {
      this.phone = parseInt(this.phone)
      if (this.driverLicence === null) {
        this.driverLicence = false
      } else {
        if (this.driverLicence=="y")
          this.driverLicence = true
        else 
          this.driverLicence = false
      }
    },
    // fonction pour hasher le mot de passe de l'utilisateur qui s'inscrit
    async hashPassword() {
      const salt = await bcryptjs.genSalt()
      this.password = await bcryptjs.hash(this.password, salt)
    },
    // fonction pour vérifier si l'adresse mail finit bien soit par "etu.univ-orleans.fr", soit par "univ-orleans.fr"
    verifMail() {
      console.log(this.mail);
      if (this.mail.endsWith('univ-orleans.fr') == false || this.mail.endsWith('etu.univ-orleans.fr') == false){
        this.errors.push("Votre adresse mail doit être sous le format 'xxx@univ-orleans.fr' ou 'xxx@etu.univ-orleans.fr'.");
      }
    },
            // Log the user in
            login() {
              this.$auth.loginWithRedirect();
            },
            // Log the user out
            logout() {
              this.$auth.logout({
                returnTo: window.location.origin
              });
            }
  },
  watch: {
    lang: function() {
      window.localStorage.setItem('lang', this.lang)
      translateAllElements(this.lang);
    }
  }
}

// translation //
let locale = window.localStorage.getItem('lang');

const translations = {
  // francais
  "fr": {
    "nav-home": "Accueil",
    "nav-route-proposal": "Proposer un trajet",
    "nav-routes-list": "Liste des itinéraires",
    "nav-login": "Connexion",
    "nav-account": "Compte",
    "nav-account-details": "Détails",
    "nav-logout": "Déconnexion",
    "nav-full-logout": "Déco totale",
    "page-title": "Créer un compte",
    "register-errors-text": "Veuillez corriger les erreurs sur le(s) champ(s) indiqué(s) :",
    "register-name": "Nom",
    "register-firstname": "Prenom",
    "register-password": "Mot de passe",
    "register-phone": "N° téléphone",
    "register-address": "Adresse",
    "Permis de conduire": "register-driver-licence",
    "register-driver-licence-yes": "Oui",
    "register-driver-licence-no": "Non",
    "register-submit": "Inscription"
  },

  // anglais
  "en": {
    "nav-home": "Home",
    "nav-route-proposal": "Propose a route",
    "nav-routes-list": "Routes list",
    "nav-login": "Login",
    "nav-account": "Account",
    "nav-account-details": "Detail",
    "nav-logout": "Logout",
    "nav-full-logout": "Full logout",
    "page-title": "Create an account",
    "register-errors-text": "Please correct the following fields errors :",
    "register-name": "Name",
    "register-firstname": "First name",
    "register-password": "Password",
    "register-phone": "Phone number",
    "register-address": "Address",
    "register-driver-licence": "Driver licence",
    "register-driver-licence-yes": "Yes",
    "register-driver-licence-no": "No",
    "register-submit": "Register"
  },
};

document.addEventListener("DOMContentLoaded", () => {
  translateAllElements(locale);
});

function translateAllElements(lang) {
  translateElementTextTrigger(lang);
  translateElementPlaceholderTrigger(lang);
  translateElementValueTrigger(lang);
}

// texts
function translateElementTextTrigger(lang) {
  document.querySelectorAll("[data-i18n-key]").forEach(elem => {
    translateElementText(elem, lang)
  });
}
function translateElementText(element, lang) {
  const key = element.getAttribute("data-i18n-key");
  const translation = translations[lang][key];
  if (translations[lang][key]!=null)
    element.innerText = translation;
}

// placeholders
function translateElementPlaceholderTrigger(lang) {
  document.querySelectorAll("[data-i18n-placeholder-key]").forEach(elem => {
    translateElementPlaceholder(elem, lang)
  });
}
function translateElementPlaceholder(element, lang) {
  const key = element.getAttribute("data-i18n-placeholder-key");
  const translation = translations[lang][key];
  if (translations[lang][key]!=null)
    element.placeholder = translation;
}

// values
function translateElementValueTrigger(lang) {
  document.querySelectorAll("[data-i18n-value-key]").forEach(elem => {
    translateElementValue(elem, lang)
  });
}
function translateElementValue(element, lang) {
  const key = element.getAttribute("data-i18n-value-key");
  const translation = translations[lang][key];
  if (translations[lang][key]!=null)
    element.value = translation;
}
