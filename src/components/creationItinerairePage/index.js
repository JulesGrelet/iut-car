import gql from "graphql-tag";

const ADD_ITINERAIRE = gql`
mutation addItineraire(
  $depart: String!
  $destination: String!
  $date_depart: timestamptz!
  $nb_passager: Int!
  $temps_trajet: float8!
  $point_depart: point!
  $point_arrivee: point!
  $localisation_depart: geography!
  $localisation_arrivee: geography!
  $description: String!
  $prix: float8!
  $date: String!
) {
  insert_itineraire(
    objects: [
      {
        depart: $depart
        destination: $destination
        date_depart: $date_depart
        nb_passager: $nb_passager
        temps_trajet: $temps_trajet
        point_depart: $point_depart
        point_arrivee: $point_arrivee
        localisation_depart: $localisation_depart
        localisation_arrivee: $localisation_arrivee
        description: $description
        prix: $prix
        date: $date
      }
    ]
  ){
    returning {
      id
    }
  }
}
`;

const ADD_UTILISATEUR_ITINERAIRE = gql`
mutation add_utilisateur_itineraire ($idItineraire: Int!, $idUtilisateur: Int!) {
  insert_utilisateurs_itineraire(objects: {itineraire: $idItineraire, utilisateur: $idUtilisateur, conducteur: true}) {
    returning {
      itineraire
    }
  }
}`;

var temps_trajet = 0;
var point_depart = null;
var point_arrivee = null;
var localisation_depart = null;
var localisation_arrivee = null;

export default {
  name: 'creationItinerairePage',
  data () {
    return {
      depart: null,
      destination : null,
      date_depart : null,
      nb_passager : null,
      temps_trajet : null,
      point_depart : null,
      point_arrivee : null,
      localisation_depart : null,
      localisation_arrivee : null,
      description : null,
      prix : null,
      date: null,
      errors: [],
      lang: 'fr'
    }
  },
  apollo: {},
  methods: {
    submit: function (e) {

      // si l'utilisateur n'est pas connecté, il est redirigé vers la page de login
      if (!localStorage.getItem('userId')){
        return this.$router.push({name: 'logInForm'})
      }

      this.date_depart = new Date($('#date_depart')[0].value)
      this.depart = $('#mapid input')[0].value
      this.destination = $('#mapid input')[$('#mapid input').length - 1].value
      this.temps_trajet = temps_trajet
      this.point_depart = point_depart
      this.point_arrivee = point_arrivee
      this.localisation_depart = localisation_depart
      this.localisation_arrivee = localisation_arrivee
      this.nb_passager = parseInt(this.nb_passager)
      
      // si les champs sont renseignés, on créé l'itinéraire et on y ajoute le conducteur
      if ($('#date_depart')[0].value !== '' && this.nb_passager && this.prix && $('.leaflet-marker-icon').length >= 2) {

        let timestamp = this.date_depart.toString()
        let year = timestamp[11]+timestamp[12]+timestamp[13]+timestamp[14]
        let month = timestamp[4]+timestamp[5]+timestamp[6]
        let day = timestamp[8]+timestamp[9]
        switch(month) {
          case 'Jan': month = "01"; break;
          case 'Feb': month = "02"; break;
          case 'Mar': month = "03"; break;
          case 'Apr': month = "04"; break;
          case 'May': month = "05"; break;
          case 'Jun': month = "06"; break;
          case 'Jul': month = "07"; break;
          case 'Aug': month = "08"; break;
          case 'Sep': month = "09"; break;
          case 'Oct': month = "10"; break;
          case 'Nov': month = "11"; break;
          case 'Dec': month = "12"; break;
        }
        this.date = year+"-"+month+"-"+day
        console.log(this.date)
        
        const { depart, destination, date_depart, nb_passager, temps_trajet, point_depart, point_arrivee, localisation_depart, localisation_arrivee, description, prix, date, errors } = this.$data;
        this.$apollo.mutate({
          mutation: ADD_ITINERAIRE,
          variables: {
            depart,
            destination,
            date_depart,
            nb_passager,
            temps_trajet,
            point_depart,
            point_arrivee,
            localisation_depart,
            localisation_arrivee,
            description,
            prix,
            date
          }
        }).then(({data}) => { 
          var idItineraire = data.insert_itineraire.returning[0].id
          var idUtilisateur = parseInt(window.localStorage.getItem('userId'))

          this.$apollo.mutate({
            mutation: ADD_UTILISATEUR_ITINERAIRE,
            variables: {
              idItineraire,
              idUtilisateur,
            }
          }).then(({data}) => { 
            // une fois l'itinéraire ajouté, on redirige l'utilisateur vers les détails de cet itinéraire
            this.$router.push({ name: 'detailsItinerairePage', params: { id: data.insert_utilisateurs_itineraire.returning[0].itineraire } })
          }).catch((error) => {
            console.log(error);
          });
        }).catch((error) => {
          console.log(error);
        });
      }

      this.errors = []

      // Affichage des messages d'erreur en cas de formulaire mal rempli
      if ($('.leaflet-marker-icon').length < 2) {
        this.errors.push('Choisissez un point de départ et un point d\'arrivée')
      }
      if ($('#date_depart')[0].value === '') {
        this.errors.push('Choisissez une date et une heure de départ')
      }
      if (!this.nb_passager) {
        this.errors.push('Choisissez le nombre de passagers possible')
      }
      if (!this.prix) {
        this.errors.push('Choisissez le prix par passager')
      }

      e.preventDefault()
    }
  },
  mounted () {
    this.lang = window.localStorage.getItem('lang')
    afficheMap(this.lang)
  },
  watch: {
    lang: function() {
      window.localStorage.setItem('lang', this.lang)
      translateAllElements(this.lang);
    }
  }
}

function afficheMap (lang) {
  var L = window.L
  var mymap = L.map('mapid').setView([47.843195, 1.926056], 17)

  // recupération du cadrage de la carte
  L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Itinéraire du trajet',
    maxZoom: 25,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'sk.eyJ1IjoibWF0dGhpZXU0OSIsImEiOiJja2t4djM4dHEwM2kzMnBxb3czam9vdXg2In0.j6qvI5SY3BNQVCUMuN4a7w'
  }).addTo(mymap)

  // ajout du geocoder et du routeur à la carte
  const control = L.Routing.control({
    routeWhileDragging: false,
    language: lang,
    reverseWaypoints: true,
    geocoder: L.Control.Geocoder.nominatim(),
    lineOptions: {
      styles: [{
        color: '#791866',
        opacity: 0.8,
        weight: 4
      }]
    },
    pointMarkerStyle: {
      radius: 5,
      color: '#791866',
      fillOpacity: 1,
      fillColor: '#767375',
    },
    router: new L.Routing.osrmv1({
      language: lang
    })
  }).addTo(mymap);
  
  // ajout marqueur de l'IUT d'Orléans
  var IUTmarker = L.marker([47.843272, 1.926727], ).addTo(mymap);

  // ajout d'un popup sur le marqueur
  IUTmarker.bindPopup("<b>IUT Orléans</b><br>16 Rue d'Issoudun\n 45100 Orléans ").openPopup();

  // fonction se déclnchant lors d'un click et permettant de créer un point
  // var popup = L.popup();

  // permet de stocker les points placer sur la map quand on clique
  let markers = L.layerGroup();

  function onMapClick(e) {
 
    var marker;

      // popup
      //     .setLatLng(e.latlng)
      //     .setContent("Vous avez cliquer sur la carte en " + e.latlng.toString())
      //     .openOn(mymap);

      // creation du marqueur  
      var marker = L.marker(e.latlng, {draggable:'true'});
      // ajout des marqueurs sur la map 
      marker.addTo(markers);
      markers.addTo(mymap);
      markers.removeLayer(marker);

      marker = new L.marker(e.latlng, {draggable:'true'});
      marker.on('dragend', function(event){
        var marker = event.target;
        var position = marker.getLatLng();
        marker.setLatLng(new L.LatLng(position.lat, position.lng),{draggable:'true'});
        mymap.panTo(new L.LatLng(position.lat, position.lng))
      });

      // lorsqu'on clique, cela créer une route de L'IUT vers l'endroit cliqué
      // control.setWaypoints([
      //   L.latLng(47.843272, 1.926727),
      //   L.latLng(marker.getLatLng())
      // ]);

      console.log("controle:"+control.getWaypoints()[0].latLng);
      console.log("condition boucle2:" + control.getWaypoints()[0].latLng !== null);

      // si il n'yy a pas de point de depart, creer le depart a l'endroit cliqué
      if (control.getWaypoints()[0].latLng == null){

      control.setWaypoints([
        L.latLng(marker.getLatLng())
      ]);
      // sinon si il y a deja un depart, creer l'arrivé sur le point cliqué
      } else if (control.getWaypoints()[0].latLng !== null){
        control.setWaypoints([
          control.getWaypoints()[0].latLng,
          L.latLng(marker.getLatLng())
        ]);
      }    

  }

  mymap.on('click', onMapClick);

  // ajout de l'autocomplétion de la recherche d'adresses (NON FONCTIONNEL)
  /*new L.Control.GPlaceAutocomplete({
    callback: function(place){
        var loc = place.geometry.location;
        mymap.panTo([loc.lat(), loc.lng()]);
        mymap.setZoom(17);
    }
  }).addTo(mymap);*/

  control.on('routeselected', function (e) {

    var p1 = [control.getWaypoints()[0].latLng.lat, control.getWaypoints()[0].latLng.lng]
    var p2 = [control.getWaypoints()[control.getWaypoints().length - 1].latLng.lat, control.getWaypoints()[control.getWaypoints().length - 1].latLng.lng]

    point_depart = `${p1[0]}, ${p1[1]}`;
    localisation_depart = {
      "type": "Point",
      "coordinates":[p1[0], p1[1]]
    };

    point_arrivee = `${p2[0]}, ${p2[1]}`;
    localisation_arrivee = {
      "type": "Point",
      "coordinates":[p2[0], p2[1]]
    };

    // recupération du temps de trajet afficher sur l'itinéraire sélectionné
    temps_trajet = control._routes[e.route.routesIndex].summary.totalTime;

  }.bind(this));
}

// translation //
let locale = window.localStorage.getItem('lang');

const translations = {
  // francais
  "fr": {
    "nav-home": "Accueil",
    "nav-route-proposal": "Proposer un trajet",
    "nav-routes-list": "Liste des itinéraires",
    "nav-login": "Connexion",
    "nav-account": "Compte",
    "nav-account-details": "Détails",
    "nav-logout": "Déconnexion",
    "nav-auth": "Authentification",
    "nav-full-logout": "Déco totale",
    "nav-userslist-input": "Chercher un utilisateur",
    "page-title": "Proposer un trajet",
    "form-seats-number": "Nombre de passagers max.",
    "form-price": "Prix",
    "form-description": "Description",
    "form-errors": "Veuillez corriger les erreur(s) suivante(s) : ",
    "form-submit": "Valider",
    "email-not-univ": "Votre compte d'authentification n'appartient pas à l'université d'Orléans.",
    "email-not-univ-auth": "Authentification",
    "email-not-univ-full-logout": "Déconnexion totale"
  },

  // anglais
  "en": {
    "nav-home": "Home",
    "nav-route-proposal": "Propose a route",
    "nav-routes-list": "Routes list",
    "nav-login": "Login",
    "nav-account": "Account",
    "nav-account-details": "Detail",
    "nav-logout": "Logout",
    "nav-auth": "Authentication",
    "nav-full-logout": "Full logout",
    "nav-userslist-input": "Find an user",
    "page-title": "Propose a route",
    "form-seats-number": "Max. number of passengers",
    "form-price": "Price",
    "form-description": "Description",
    "form-errors": "Please correct the following error(s) : ",
    "form-submit": "Submit",
    "email-not-univ": "Your authentication account does not belong to the Orléans University.",
    "email-not-univ-auth": "Authentication",
    "email-not-univ-full-logout": "Full logout"
  },
};

document.addEventListener("DOMContentLoaded", () => {
  translateAllElements(locale);
});

function translateAllElements(lang) {
  translateElementTextTrigger(lang);
  translateElementPlaceholderTrigger(lang);
  translateElementValueTrigger(lang);
}

// texts
function translateElementTextTrigger(lang) {
  document.querySelectorAll("[data-i18n-key]").forEach(elem => {
    translateElementText(elem, lang)
  });
}
function translateElementText(element, lang) {
  const key = element.getAttribute("data-i18n-key");
  const translation = translations[lang][key];
  if (translations[lang][key]!=null)
    element.innerText = translation;
}

// placeholders
function translateElementPlaceholderTrigger(lang) {
  document.querySelectorAll("[data-i18n-placeholder-key]").forEach(elem => {
    translateElementPlaceholder(elem, lang)
  });
}
function translateElementPlaceholder(element, lang) {
  const key = element.getAttribute("data-i18n-placeholder-key");
  const translation = translations[lang][key];
  if (translations[lang][key]!=null)
    element.placeholder = translation;
}

// values
function translateElementValueTrigger(lang) {
  document.querySelectorAll("[data-i18n-value-key]").forEach(elem => {
    translateElementValue(elem, lang)
  });
}
function translateElementValue(element, lang) {
  const key = element.getAttribute("data-i18n-value-key");
  const translation = translations[lang][key];
  if (translations[lang][key]!=null)
    element.value = translation;
}
