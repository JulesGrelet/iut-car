import gql from "graphql-tag"
import userProfile from '@/components/userProfilePage/userProfilePage'

const GET_USERS_0 = gql `
  query MyQuery {
  utilisateurs {
    nom
    prenom
    id
    created_at
  }
}
`;

const GET_USERS = gql `
query MyQuery($text: String!) {
  utilisateurs(where: {_or:[{prenom: {_ilike: $text}}, {nom: {_ilike: $text}}]}) {
    nom
    prenom
    id
  }
}
`;

export default {
    data() {
        return {
            text: this.$route.params.text,
            users: [],
            fieldsFr: [
                { key: 'prenom', label: 'Prenom' },
                { key: 'nom', label: 'Nom' },
                { key: 'actions', label: 'Infos' }
            ],
            fieldsEn: [
                { key: 'prenom', label: 'First name' },
                { key: 'nom', label: 'Name' },
                { key: 'actions', label: 'Infos' }
            ],
            search: '',
            show: false,
            display: false,
            lang: 'fr'
        }
    },
    mounted() {
        this.lang = window.localStorage.getItem('lang')
    },
    apollo: {},
    watch: {
        search: function() {
            if (this.search == '') {
                this.show = null
            } else {
                this.query()
                this.show = true
            }
        }
    },
    methods: {
        info(item, index, button) {
            this.infoModal.title = `Row index: ${index}`
            this.infoModal.content = JSON.stringify(item, null, 2)
            this.$root.$emit('bv::show::modal', this.infoModal.id, button)
        },
        resetInfoModal() {
            this.infoModal.title = ''
            this.infoModal.content = ''
        },
        query() {
            const text = '%' + this.search + '%'
            this.$apollo.query({
                query: GET_USERS,
                variables: {
                    text
                }
            }).then(({ data }) => {
                this.users = data.utilisateurs;
            }).catch((error) => {
                console.log(error);
            });
            this.display = true
        }
    },
    components: {
        'userProfile': userProfile
    }
}