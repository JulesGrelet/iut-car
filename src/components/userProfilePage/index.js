import gql from "graphql-tag"
import Rating from '../Rating/Rating'

const GET_USER_BY_ID = gql`
  query MyQuery ($id: Int!) {
  utilisateurs(where: {id: {_eq: $id}}) {
    adresse
    composante
    description
    email
    nom
    permisConduire
    photo
    prenom
    telephone
  }
}
`;

const GET_NOTATION_BY_DRIVER_USER = gql`
query get_notation_by_driver_user ($driver: Int!) {
  notation (where: {utilisateurConducteur: {_eq: $driver} }) {
    note
  }
}
`;

export default {
  data() {
    return {
      user: [],
      userId: this.$route.params.userId,
      userExist: true,
      ratingAverage: null,
      ratingAveragePrecise: null,
      starsList: [],
      lang: 'fr'
    }
  },
  apollo: {},
  components: { Rating },
  mounted() {
    this.lang = window.localStorage.getItem('lang')

    const id = this.$data.userId
    this.$apollo.query({
      query: GET_USER_BY_ID,
      variables: {
        id
      }
    }).then(({data}) => {
      if (data.utilisateurs[0]!=null) {
        this.user = data.utilisateurs[0]
      } else {
        this.userExist = false
      }
    }).catch((error) => {
      console.log(error);
    });

    const driver = id
    this.$apollo.query({
      query: GET_NOTATION_BY_DRIVER_USER,
      variables: {
        driver
      }
    }).then(({data}) => {
      if (data.notation.length>0) {
        let sum = 0;
        for (let notation of data.notation) {
          sum+=notation.note
        }
        this.ratingAverage = sum / data.notation.length
      }
      if (this.ratingAverage!=null) {
        this.ratingAveragePrecise = this.ratingAverage.toFixed(1)
        this.ratingAverage = parseInt(this.ratingAverage.toString(), 10)
        for (let i=0; i<5; i++) {
          if (i<this.ratingAverage) {
            this.starsList[i] = {id: i, img: "star", width: 30, alt: "*"}
          } else if (i==this.ratingAverage) {
            if (this.ratingAveragePrecise<i+0.5)
              this.starsList[i] = {id: i, img: "star_empty", width: 23, alt: "°"}
            else
              this.starsList[i] = {id: i, img: "star_half", width: 30, alt: "^"}
          } else if (i>this.ratingAverage) {
            this.starsList[i] = {id: i, img: "star_empty", width: 23, alt: "°"}
          }
        }
      }
    }).catch((error) => {
      console.log(error);
    });
  },
  watch: {
    lang: function() {
      //window.localStorage.setItem('lang', this.lang)
      console.log(this.lang)
      //translateAllElements(this.lang);
    }
  }
}

// translation //
let locale = window.localStorage.getItem('lang');

const translations = {
  // francais
  "fr": {
    "nav-home": "Accueil",
    "nav-route-proposal": "Proposer un trajet",
    "nav-routes-list": "Liste des itinéraires",
    "nav-login": "Connexion",
    "nav-account": "Compte",
    "nav-account-details": "Détails",
    "nav-logout": "Déconnexion",
    "nav-full-logout": "Déco totale",
    "rating-no-rating": "Aucune note",
    "phone-number": "Telephone",
    "address": "Adresse",
    "driver-licence": "Permis conduire",
    "btn-details": "Détails",
    "unknown-user": "Utilisateur inconnu",
    "email-not-univ": "Votre compte d'authentification n'appartient pas à l'université d'Orléans.",
    "email-not-univ-login": "Authentification",
    "email-not-univ-full-logout": "Déconnexion totale"
  },

  // anglais
  "en": {
    "nav-home": "Home",
    "nav-route-proposal": "Propose a route",
    "nav-routes-list": "Routes list",
    "nav-login": "Login",
    "nav-account": "Account",
    "nav-account-details": "Detail",
    "nav-logout": "Logout",
    "nav-full-logout": "Full logout",
    
    "rating-no-rating": "Never rated",
    "phone-number": "Phone number",
    "address": "Address",
    "driver-licence": "Driver licence",
    "btn-details": "Details",
    "unknown-user": "Unknown user",
    "email-not-univ": "Your authentication account does not belong to the Orléans University.",
    "email-not-univ-auth": "Authentication",
    "email-not-univ-full-logout": "Full logout"
  },
};

document.addEventListener("DOMContentLoaded", () => {
  translateAllElements(locale);
});

function translateAllElements(lang) {
  translateElementTextTrigger(lang);
}

// texts
function translateElementTextTrigger(lang) {
  document.querySelectorAll("[data-i18n-key]").forEach(elem => {
    translateElementText(elem, lang)
  });
}
function translateElementText(element, lang) {
  const key = element.getAttribute("data-i18n-key");
  const translation = translations[lang][key];
  if (translations[lang][key]!=null)
    element.innerText = translation;
}
