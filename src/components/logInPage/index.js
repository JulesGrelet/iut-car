import gql from "graphql-tag"
const bcryptjs = require('bcryptjs')
import VueSession from 'vue-session'
import Vue from "vue";
Vue.use(VueSession)

const GET_LOGS = gql`
  query MyQuery($email: String) {
    utilisateurs(where: {email: {_eq: $email}}) {
      id
      password
  }
}
`
export default {
  name: 'logInForm',
  data() {
    return {
      errors: [],
      email: null,
      password: null,
      lang: 'fr'
    }
  },
  mounted() {
    this.lang = window.localStorage.getItem('lang')
  },
  appollo: {},
  methods:{
    checkForm: function (e) {
      e.preventDefault()

      this.errors = []

      if (this.email === '') {
        this.errors.push('Veuillez renseigner votre email')
      } else if(this.password === '') {
        this.errors.push('Veuillez renseigner votre mot de passe')
        return false
      }
      else {
        let email = this.email
        this.$router.push({name: 'WelcomePage'})
        this.$apollo.query({
          query: GET_LOGS,
          variables: {
            email
          }
        }).then(async res => {
          if (!res.data.utilisateurs.length) {
            this.errors.push('Veuillez renseigner un email valide')
            return false
          }
          let password = res.data.utilisateurs[0].password
          bcryptjs.compare(this.password, password, function(err, result) {
            if (result) {
              if (res.data.utilisateurs[0].id) {
                localStorage.setItem('userId', res.data.utilisateurs[0].id);
                location.reload()
              }
              else {
                console.log('id doesn\'t exist')
              }
            } else {
              console.log('connection failed')
            }
          });
        })
      }
    },
    register: function (e) {
      e.preventDefault()
      this.$router.push({name: 'signInForm'})
    }
  },
  watch: {
    lang: function() {
      window.localStorage.setItem('lang', this.lang)
      translateAllElements(this.lang);
    }
  }
}

// translation //
let locale = window.localStorage.getItem('lang');

const translations = {
  // francais
  "fr": {
    "nav-home": "Accueil",
    "nav-route-proposal": "Proposer un trajet",
    "nav-routes-list": "Liste des itinéraires",
    "nav-login": "Connexion",
    "nav-account": "Compte",
    "nav-account-details": "Détails",
    "nav-logout": "Déconnexion",
    "nav-full-logout": "Déco totale",
    "page-title": "Connexion",
    "login-errors-text": "Veuillez corriger les erreur(s) suivante(s) : ",
    "login-password": "Mot de passe",
    "login-btn-login": "Connexion",
    "register": "Inscription"
  },

  // anglais
  "en": {
    "nav-home": "Home",
    "nav-route-proposal": "Propose a route",
    "nav-routes-list": "Routes list",
    "nav-login": "Login",
    "nav-account": "Account",
    "nav-account-details": "Detail",
    "nav-logout": "Logout",
    "nav-full-logout": "Full logout",
    "page-title": "Login",
    "login-errors-text": "Please correct the following error(s) : ",
    "login-password": "Password",
    "login-btn-login": "Login",
    "register": "Register"
  },
};

document.addEventListener("DOMContentLoaded", () => {
  translateAllElements(locale);
});

function translateAllElements(lang) {
  translateElementTextTrigger(lang);
  translateElementPlaceholderTrigger(lang);
  translateElementValueTrigger(lang);
}

// texts
function translateElementTextTrigger(lang) {
  document.querySelectorAll("[data-i18n-key]").forEach(elem => {
    translateElementText(elem, lang)
  });
}
function translateElementText(element, lang) {
  const key = element.getAttribute("data-i18n-key");
  const translation = translations[lang][key];
  if (translations[lang][key]!=null)
    element.innerText = translation;
}

// placeholders
function translateElementPlaceholderTrigger(lang) {
  document.querySelectorAll("[data-i18n-placeholder-key]").forEach(elem => {
    translateElementPlaceholder(elem, lang)
  });
}
function translateElementPlaceholder(element, lang) {
  const key = element.getAttribute("data-i18n-placeholder-key");
  const translation = translations[lang][key];
  if (translations[lang][key]!=null)
    element.placeholder = translation;
}

// values
function translateElementValueTrigger(lang) {
  document.querySelectorAll("[data-i18n-value-key]").forEach(elem => {
    translateElementValue(elem, lang)
  });
}
function translateElementValue(element, lang) {
  const key = element.getAttribute("data-i18n-value-key");
  const translation = translations[lang][key];
  if (translations[lang][key]!=null)
    element.value = translation;
}
