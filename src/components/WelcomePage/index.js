import gql from "graphql-tag";
import {Library} from '../../Library'

/* eslint-disable */

const GET_NOTE_UTILISATEUR = gql`
query get_note_utilisateur($id: Int!) {
  notation_aggregate(where: {utilisateurConducteur: {_eq: $id}}) {
    aggregate {
      avg {
        note
      }
    }
  }
}
`;

const RECHERCHE_ITINERAIRE = gql`
query recherche_itineraire($depart_carte: geography!, $distance: Float!, $arrivee_carte: geography!, $nb_passager: Int!, $date_depart: timestamptz!, $date_depart_tard: timestamptz!) {
  utilisateurs_itineraire(where: {itineraireByItineraire: {localisation_depart: {_st_d_within: {distance: $distance, from: $depart_carte}}, localisation_arrivee: {_st_d_within: {distance: $distance, from: $arrivee_carte}}, nb_passager: {_gte: $nb_passager}, date_depart: {_gte: $date_depart}, _and: {date_depart: {_lte: $date_depart_tard}}}, conducteur: {_eq: true}}, order_by: {itineraireByItineraire: {date_depart: asc}}) {
    itineraireByItineraire {
      id
      destination
      description
      depart
      date_depart
      localisation_arrivee
      localisation_depart
      nb_passager
      point_arrivee
      point_depart
      prix
      temps_trajet
    }
    utilisateurByUtilisateur {
      id
      nom
      prenom
      permisConduire
      description
      email
      created_at
      composante
      telephone
      libelle
      marque
      modele
      nb_place
      photo
    }
  }
}
`;

const GET_DEPART_AUTOCOMPLETE = gql`
query get_depart_autocomplete($depart: String!) {
  itineraire(distinct_on: [depart], where: {depart: {_ilike: $depart}}, order_by: [{depart: asc}]) {
    id
    depart
  }
}
`;

const GET_DESTINATION_AUTOCOMPLETE = gql`
query get_destination_autocomplete($destination: String!) {
  itineraire(distinct_on: [destination], where: {destination: {_ilike: $destination}}, order_by: [{destination: asc}]) {
    id
    destination
  }
}
`;

export default {
  name: 'WelcomePage',
  data () {
    return {
      date_depart : new Date().toISOString().substr(0, 10),
      nb_passager : 1,
      perimetre : 5,
      list_itineraire: [],
      result_msg: null,
      errors: [],
      depart: "",
      autocompleteDepartsList: [],
      destination: "",
      autocompleteDestinationsList: [],
      lang: 'fr'
    }
  },
  apollo: {},
  methods: {
    autocompleteDepartUpdate() {
      const depart = '%'+this.depart+'%'
      this.$apollo.query({
        query: GET_DEPART_AUTOCOMPLETE,
        variables: {
          depart
        }
      }).then(({data}) => {
        this.autocompleteDepartsList = data.itineraire
      }).catch((error) => {
        console.log(error);
      });
    },
    autocompleteDestinationUpdate() {
      const destination = '%'+this.destination+'%'
      this.$apollo.query({
        query: GET_DESTINATION_AUTOCOMPLETE,
        variables: {
          destination
        }
      }).then(({data}) => {
        this.autocompleteDestinationsList = data.itineraire
      }).catch((error) => {
        console.log(error);
      });
    },
    submit: function (e) {
      this.date_depart = this.date_depart
      this.depart = this.depart
      this.destination = this.destination
      this.nb_passager = parseInt(this.nb_passager)
      this.perimetre = parseInt(this.perimetre)
      this.result_msg = ''
      this.list_itineraire = []

      if (this.depart && this.destination && this.date_depart) {
        
        Library.get_lat_lon(this.depart, this.destination).then((data) => {
          var depart_carte = data[0]
          var arrivee_carte = data[1]
          var distance = this.perimetre * 1000
          var date_depart = `${this.date_depart}`
          var date_depart_tard = `${this.date_depart.slice(0, 11)}T23:59`
          var nb_passager = this.nb_passager
          
          this.$apollo.query({
            query: RECHERCHE_ITINERAIRE,
            variables: {
              depart_carte,
              arrivee_carte,
              distance,
              date_depart,
              date_depart_tard,
              nb_passager
            }
          }).then(({data}) => {
            this.list_itineraire = data.utilisateurs_itineraire
            this.list_itineraire.forEach(i => {
              var id = i.utilisateurByUtilisateur.id
              this.$apollo.query({
                query: GET_NOTE_UTILISATEUR,
                variables: {
                  id,
                }
              }).then(({data}) => {
                if (data.notation_aggregate.aggregate.avg.note) {
                  i.utilisateurByUtilisateur['note_moy'] = `${data.notation_aggregate.aggregate.avg.note.toFixed(1)}/5`
                }else{
                  i.utilisateurByUtilisateur['note_moy'] = 'aucune note'
                }
                this.$forceUpdate();
              }).catch((error) => {
                console.log(error);
              });
            });
            localStorage.setItem('depart', this.depart);
            localStorage.setItem('arrivee', this.destination);
            localStorage.setItem('date_depart', this.date_depart);
            localStorage.setItem('nb_passager', this.nb_passager);
            if (this.lang=='en')
              this.result_msg = `No route from ${this.depart} to ${this.destination} on ${Library.transform_date(this.date_depart)} for ${this.nb_passager} passenger(s)`
            else
              this.result_msg = `Aucun trajet de ${this.depart} à ${this.destination} le ${Library.transform_date(this.date_depart)} pour ${this.nb_passager} passager(s)`
          }).catch((error) => {
            console.log(error);
          });
        })
      }

      this.errors = []

      if (!this.depart) {
        this.errors.push('Choisissez un lieu de départ')
      }
      if (!this.destination) {
        this.errors.push('Choisissez un lieu d\'arrivée')
      }
      if (!this.date_depart) {
        this.errors.push('Choisissez une date de départ')
      }
      e.preventDefault()
    },
  },
  filters: {
    format_date_heure: function (value) {
      return Library.transform_date_heure(value)
    },
    format_date: function (value) {
      return Library.transform_date(value)
    },
    format_duree: function (value) {
      return Library.get_duree_from_sec(value)
    },
    get_first_letter: function (value) {
      return value.slice(0, 1) + "."
    },
    get_city: function (value) {
      return value.split(",")[0]
    }
  },
  mounted () {
    this.lang = window.localStorage.getItem('lang')
    
    var depart = localStorage.getItem('depart');
    var arrivee = localStorage.getItem('arrivee');
    var date_depart = localStorage.getItem('date_depart');
    var nb_passager = localStorage.getItem('nb_passager');
    
    if (depart) {this.depart = depart}
    if (arrivee) {this.destination = arrivee}
    if (date_depart) {this.date_depart = date_depart}
    if (nb_passager) {this.nb_passager = nb_passager}
    if (depart && arrivee && date_depart && nb_passager) {
      $('#rechercher').trigger('click')
    }
  },
  watch: {
    depart: function() {
      if (this.depart!="" && this.depart!=" ")
        this.autocompleteDepartUpdate();
      else
        this.autocompleteDepartsList=[]
    },
    destination: function() {
      if (this.destination!="" && this.destination!=" ")
        this.autocompleteDestinationUpdate();
      else
        this.autocompleteDestinationsList=[]
    },
    lang: function() {
      window.localStorage.setItem('lang', this.lang)
      translateAllElements(this.lang);
    }
  }
}

// translation //
let locale = window.localStorage.getItem('lang');

const translations = {
  // francais
  "fr": {
    "nav-home": "Accueil",
    "nav-route-proposal": "Proposer un trajet",
    "nav-routes-list": "Liste des itinéraires",
    "nav-login": "Connexion",
    "nav-account": "Compte",
    "nav-account-details": "Détails",
    "nav-logout": "Déconnexion",
    "nav-full-logout": "Déco totale",
    "search-route-departure": "Départ",
    "search-route-destination": "Arrivée",
    "search-route-date": "Date",
    "search-route-passengers": "Nombre de passagers",
    "search-route-perimeter": "Périmètre de recherche",
    "search-route-errors-text": "Veuillez corriger les erreurs suivantes : ",
    "route-user-details": "Détails",
    "route-user-car-no-car": "véhicule inconnu",
    "route-passengers-text": "passager(s)",
    "route-price-text": "€ par passager",
    "route-description-no-description": "Aucune description",
    "route-details": "Détails",
    "card-infos-1-title": "Vos trajets préférés à petits prix où que vous alliez",
    "card-infos-1-text": "En covoiturage, trouvez le trajet idéal parmi notre large choix de destinations à petits prix.",
    "card-infos-2-title": "Voyagez en toute confiance",
    "card-infos-2-text": "Nous prenons le temps qu’il faut pour connaître nos membres et nos compagnies de bus partenaires. Nous vérifions les avis, les profils et les pièces d’identité. Vous savez donc avec qui vous allez voyager pour réserver en toute confiance sur notre plateforme sécurisée.",
    "card-infos-3-title": "Recherchez, cliquez et réservez !",
    "card-infos-3-text": "Réserver un trajet devient encore plus simple ! Facile d'utilisation et dotée de technologies avancées, notre appli vous permet de réserver un trajet à proximité en un rien de temps.",
    "email-not-univ": "Votre compte d'authentification n'appartient pas à l'université d'Orléans.",
    "email-not-univ-login": "Authentification",
    "email-not-univ-full-logout": "Déconnexion totale"
  },

  // anglais
  "en": {
    "nav-home": "Home",
    "nav-route-proposal": "Propose a route",
    "nav-routes-list": "Routes list",
    "nav-login": "Login",
    "nav-account": "Account",
    "nav-account-details": "Detail",
    "nav-logout": "Logout",
    "nav-full-logout": "Full logout",   
    "search-route-departure": "Departure",
    "search-route-destination": "Destination",
    "search-route-date": "Date",
    "search-route-passengers": "Passengers number",
    "search-route-perimeter": "Search scope",
    "search-route-errors-text": "Please correct the following errors :",
    "route-user-details": "Details",
    "route-user-car-no-car": "Unknown vehicle",
    "route-passengers-text": "passenger(s)",
    "route-price-text": "€ per passenger",
    "route-description-no-description": "No description",
    "route-details": "Details",
    "card-infos-1-title": "Your favorite low-cost routes wherever you go",
    "card-infos-1-text": "With carpooling, find the ideal route from our wide choice of low-cost destinations.",
    "card-infos-2-title": "Move out with certitude",
    "card-infos-2-text": "We take time to know our members and partner bus companies. We check reviews, profiles and identities document. So you know who you are going to move out with to book with confidence on our secure platform.",
    "card-infos-3-title": "Search, click and book !",
    "card-infos-3-text": "Booking a route becomes even easier! Easy to use and equipped with advanced technologies, our app allows you to book a route nearby in no time.",
    "email-not-univ": "Your authentication account does not belong to the Orléans University.",
    "email-not-univ-auth": "Authentication",
    "email-not-univ-full-logout": "Full logout"
  },
};

document.addEventListener("DOMContentLoaded", () => {
  translateAllElements(locale);
});

function translateAllElements(lang) {
  translateElementTextTrigger(lang);
  translateElementPlaceholderTrigger(lang);
  translateElementTitleTrigger(lang);
}

// texts
function translateElementTextTrigger(lang) {
  document.querySelectorAll("[data-i18n-key]").forEach(elem => {
    translateElementText(elem, lang)
  });
}
function translateElementText(element, lang) {
  const key = element.getAttribute("data-i18n-key");
  const translation = translations[lang][key];
  if (translations[lang][key]!=null)
    element.innerText = translation;
}

// placeholders
function translateElementPlaceholderTrigger(lang) {
  document.querySelectorAll("[data-i18n-placeholder-key]").forEach(elem => {
    translateElementPlaceholder(elem, lang)
  });
}
function translateElementPlaceholder(element, lang) {
  const key = element.getAttribute("data-i18n-placeholder-key");
  const translation = translations[lang][key];
  if (translations[lang][key]!=null)
    element.placeholder = translation;
}

// titles
function translateElementTitleTrigger(lang) {
  document.querySelectorAll("[data-i18n-title-key]").forEach(elem => {
    translateElementTitle(elem, lang)
  });
}
function translateElementTitle(element, lang) {
  const key = element.getAttribute("data-i18n-title-key");
  const translation = translations[lang][key];
  if (translations[lang][key]!=null)
    element.title = translation;
}
