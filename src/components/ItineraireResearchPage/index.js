import gql from "graphql-tag"
import detailsItineraire from '@/components/detailsItinerairePage/detailsItinerairePage'
import listeItineraire from '@/components/listeItinerairePage/listeItinerairePage'

const GET_ITINERAIRE_0 = gql`
  query MyQuery {
  itineraire {
    depart
    destination
  }
}
`;

const GET_ITINERAIRE = gql`
query MyQuery($text: String!) {
  itineraire(where: {_or:[{depart: {_ilike: $text}}, {destination: {_ilike: $text}}]}) {
    depart
    destination
    id
  }
}
`;

export default {
  data() {
    return {
      text: this.$route.params.text,
      itineraires: [],
      fields:[
        { key: 'depart', label: 'Depart'},
        { key: 'destination', label: 'Destination'},
        { key: 'actions', label: 'Details' }
      ],
      search: '',
      show: false,
      display: false
    }
  },
  apollo: {},
  watch: {
    search: function () {
      if (this.search == '') {
        this.show = null
      }
      else{
        this.query()
        this.show = true
      }
    }
  },
  methods: {
    info(item, index, button) {
        this.infoModal.title = `Row index: ${index}`
        this.infoModal.content = JSON.stringify(item, null, 2)
        this.$root.$emit('bv::show::modal', this.infoModal.id, button)
    },
    resetInfoModal() {
      this.infoModal.title = ''
      this.infoModal.content = ''
    },
    query(){
      const text = '%'+this.search+'%'
      this.$apollo.query({
        query: GET_ITINERAIRE,
        variables: {
          text
        }
      }).then(({data}) => {
        this.itineraires = data.itineraire;
      }).catch((error) => {
        console.log(error);
      });
      this.display = true
    }
  },
  components: {
    'detailsItineraire': detailsItineraire,
    'listeItineraire': listeItineraire
  }
}
