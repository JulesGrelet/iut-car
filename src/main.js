// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.4

/* eslint-disable */
import Vue from 'vue'
import App from './App'
import router from './router'
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import VueApollo from 'vue-apollo';

// Import the Auth0 configuration
import { domain, clientId } from "../auth_config.json";

// Import the plugin here
import { Auth0Plugin } from "./auth";

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import { BootstrapIconsPlugin } from 'bootstrap-icons-vue';
//import css files
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// Install BootstrapVue
Vue.use(BootstrapVue)
Vue.use(BootstrapIconsPlugin);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.config.productionTip = false;

// Install the authentication plugin here
Vue.use(Auth0Plugin, {
    domain,
    clientId,
    onRedirectCallback: appState => {
        router.push(
            appState && appState.targetUrl ?
            appState.targetUrl :
            window.location.pathname
        );
    }
});

Vue.config.productionTip = false;

const httpLink = new HttpLink({
    uri: 'https://precise-goshawk-67.hasura.app/v1/graphql'
});

const apolloClient = new ApolloClient({
    link: httpLink,
    cache: new InMemoryCache(),
    connectToDevTools: true
});


Vue.use(VueApollo);

const apolloProvider = new VueApollo({
    defaultClient: apolloClient
});

//console.log(window.localStorage.getItem('lang'))
//window.localStorage.setItem('lang', "fr")
/*window.dispatchEvent(new CustomEvent('lang-localstorage-changed', {
    detail: {
      storage: window.localStorage.getItem('lang')
    }
}));*/

new Vue({
    el: '#app',
    router,
    apolloProvider,
    render: h => h(App)
}).$mount("#app");


/*=============== SHOW MENU ===============*/
const navMenu = document.getElementById('nav-menu'),
    navToggle = document.getElementById('nav-toggle'),
    navClose = document.getElementById('nav-close');

/* ===== MENU SHOW ===== */
/* Validate if constant exists */
if (navToggle) {
    navToggle.addEventListener('click', () => {
        navMenu.classList.add('show-menu');
    });
}
/* ===== MENU HIDDEN ===== */
/* Validate if constant exists */
if (navClose) {
    navClose.addEventListener('click', () => {
        navMenu.classList.remove('show-menu');
    });
}

/*=============== REMOVE MENU MOBILE ===============*/
const navLink = document.querySelectorAll('.nav__link');

function linkAction() {
    const navMenu = document.getElementById('nav-menu');
    navMenu.classList.remove('show-menu');
}

navLink.forEach(n => n.addEventListener('click', linkAction));